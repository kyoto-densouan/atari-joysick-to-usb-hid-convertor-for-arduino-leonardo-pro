# README #

ATARI joysick to USB HID Convertor for Arduino Leonardo/pro micro.  

Written by catsin.  
2017/09  

##Use Library##
import HID joystick library.  
https://github.com/MHeironimus/ArduinoJoystickLibrary/tree/version-1.0  
MHeironimus/ArduinoJoystickLibrary, joystick2  

##Wiring##
ATARI Joystick port 1 - Arduino Leonardo/pro micro  
   1 -  pin2  
   2 -  pin3  
   3 -  pin4  
   4 -  pin5  
   5 -  5V  
   6 -  pin6  
   7 -  pin7  
   8 -  GND  
   9 -  (open)  
ATARI Joystick port 2 - Arduino Leonardo/pro micro  
   1 -  pinA0  
   2 -  pinA1  
   3 -  pinA2  
   4 -  pinA3  
   5 -  5V  
   6 -  pinA4  
   7 -  pinA5  
   8 -  GND  
   9 -  (open)  
Rapid fire control button - Arduino Leonardo/pro micro  
   1 -  pin12  
   2 -  GND  
   