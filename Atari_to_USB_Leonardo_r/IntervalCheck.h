// Written by catsin.
// 2017/09

#ifndef D_INTERVAL_CHECK_H
#define D_INTERVAL_CHECK_H

// 間隔測定クラス
class  IntervalCheck
{
private:
  unsigned long m_lastTime;
  unsigned long m_intervalTime;
  bool  m_autoReset;

public:
  // コンストラクタ
  IntervalCheck(){
    setParam( 1000, false );
  }

  // コンストラクタ
  IntervalCheck( unsigned long interval, bool autoReset=true ){
    setParam( interval, autoReset );
  }

  // パラメータ設定
  void setParam( unsigned long interval, bool autoReset ){
    m_intervalTime = interval;
    m_autoReset = autoReset;
    reset();
  }

  // 時間経過判定
  bool  check(){
    bool  result = false;
    
    if ((millis()-m_lastTime)>m_intervalTime) {
      if( m_autoReset == true ){
        // オートリセットが設定されいる場合、ここで自動リセット＝リピートする
        reset();
      }
      result = true;
    }    
    return result;
  }

  // 外部からのリセット  
  void  reset(){
    m_lastTime = millis();
  }
};

#endif  // D_INTERVAL_CHECK_H



