// ATARI joysick to USB HID Convertor for Arduino Leonardo/pro micro.
// Written by catsin.
// 2017/09

// import HID joystick library.
// https://github.com/MHeironimus/ArduinoJoystickLibrary/tree/version-1.0
// MHeironimus/ArduinoJoystickLibrary, joystick2

// Wiring
// ATARI Joystick port 1 - Arduino
//   1 -  pin2
//   2 -  pin3
//   3 -  pin4
//   4 -  pin5
//   5 -  5V
//   6 -  pin6
//   7 -  pin7
//   8 -  GND
//   9 -  (open)
// ATARI Joystick port 2 - Arduino
//   1 -  pinA0
//   2 -  pinA1
//   3 -  pinA2
//   4 -  pinA3
//   5 -  5V
//   6 -  pinA4
//   7 -  pinA5
//   8 -  GND
//   9 -  (open)
// Rapid fire control button - Arduino
//   1 -  pin12
//   2 -  GND

#include "Joystick2.h"
#include "IntervalCheck.h"

// Use HID joystick device class
class SixTerminalJoystickDevice
{
protected:
  int joystickId_ = -1;
public:
  SixTerminalJoystickDevice(){
  }

  SixTerminalJoystickDevice( int joystickId ){
    begin( joystickId );
  }

  ~SixTerminalJoystickDevice(){
    end();
  }

  void begin( int joystickId ){
    joystickId_ = joystickId;    
    if(joystickId_>=0){
      Joystick[joystickId_].begin();
    }
  }

  void end(){
    if(joystickId_>=0){
      Joystick[joystickId_].end();
    }
    joystickId_ = -1;
  }

  void setXAxis(int8_t value){
    if(joystickId_>=0){
      Joystick[joystickId_].setXAxis(value);
    }
  }
  void setYAxis(int8_t value){
    if(joystickId_>=0){
      Joystick[joystickId_].setYAxis(value);
    }
  }

  void setButton(uint8_t button, uint8_t value){
    if(joystickId_>=0){
      Joystick[joystickId_].setButton(button, value);
    }
  }
  
  void pressButton(uint8_t button){
    if(joystickId_>=0){
      Joystick[joystickId_].pressButton(button);
    }
  }
  
  void releaseButton(uint8_t button){
    if(joystickId_>=0){
      Joystick[joystickId_].releaseButton(button);
    }
  }

  void update(){
  }
};

// Use HID joystick device with rapid fire class
class SixTerminalJoystickRapidDevice : public SixTerminalJoystickDevice
{
protected:
  int cnt_ = 0;
  const static int triggerNum_ = 2;
  IntervalCheck * pTriggerInterval_[triggerNum_];
  int triggerCnt_[triggerNum_];
  bool rapidEnable_[triggerNum_];
  int intervalTime_ = 100;
  
public:
  SixTerminalJoystickRapidDevice(){
    initTrriger();
  }

  SixTerminalJoystickRapidDevice( int joystickId ){
    initTrriger();
  }

  void initTrriger(){
    for( int i=0; i < triggerNum_; i++ ){
      pTriggerInterval_[i] = NULL;
      triggerCnt_[i] = 0;
      rapidEnable_[i] = false;
    }
  }

  void pressButton(uint8_t button){
    SixTerminalJoystickDevice::pressButton( button );
    if(joystickId_>=0){
      if( rapidEnable_[button] == true ){
        if( pTriggerInterval_[button] == NULL ){
          pTriggerInterval_[button] = new IntervalCheck( intervalTime_, true );
          triggerCnt_[button] = 0;
        }
      }
    }
  }
  
  void releaseButton(uint8_t button){
    if(joystickId_>=0){
      if( pTriggerInterval_[button] != NULL ){
        delete( pTriggerInterval_[button] );
      }
      pTriggerInterval_[button] = NULL;
      triggerCnt_[button] = 0;
    }
    SixTerminalJoystickDevice::releaseButton( button );
  }
  
  void update(){
    for( int i=0; i < triggerNum_; i++ ){
      if(pTriggerInterval_[i] != NULL){
        if(pTriggerInterval_[i]->check() == true){
          if((triggerCnt_[i]%2) == 0){
            SixTerminalJoystickDevice::releaseButton( i );
          }else{
            SixTerminalJoystickDevice::pressButton( i );
          }
          triggerCnt_[i]++;
        }
      }
    }
  }

  void setRapidEnable( int button, bool enable ){
    rapidEnable_[button] = enable;
  }

  void flipRapidEnable( int button ){
    if( rapidEnable_[button] == true ){
      rapidEnable_[button] = false;
    }else{
      rapidEnable_[button] = true;
    }
  }
  
};

// ATARI joystick class
class SixTerminalJoystick
{
protected:
  static const int pinNum_ = 6;
  int pinNo_[pinNum_]     = {2,3,4,5,6,7};
  int pinStatus_[pinNum_] = {LOW, LOW, LOW, LOW, LOW, LOW};
  static const int S_PIN_TYPE_STICK = 0;
  static const int S_PIN_TYPE_BUTTON = 1;
  int pinType_[pinNum_]   = {S_PIN_TYPE_STICK, S_PIN_TYPE_STICK, S_PIN_TYPE_STICK, S_PIN_TYPE_STICK, S_PIN_TYPE_BUTTON, S_PIN_TYPE_BUTTON};
  int pinId_[pinNum_]     = {0,1,2,3,0,1};
  //SixTerminalJoystickDevice myJoystickDevice_;
  SixTerminalJoystickRapidDevice myJoystickDevice_;
  
public:
 SixTerminalJoystick(){
 }

 SixTerminalJoystick( int joystickId, int pinNo0, int pinNo1, int pinNo2, int pinNo3, int pinNo4, int pinNo5 ){
  pinNo_[0] = pinNo0;
  pinNo_[1] = pinNo1;
  pinNo_[2] = pinNo2;
  pinNo_[3] = pinNo3;
  pinNo_[4] = pinNo4;
  pinNo_[5] = pinNo5;

  myJoystickDevice_.begin(joystickId);

  int i;
  for(i=0;i<pinNum_;i++){
    pinMode(pinNo_[i], INPUT_PULLUP);
  }
 }
 
 ~SixTerminalJoystick(){
  myJoystickDevice_.end();
 }

 bool update(){
  bool input = false;
  int i;
  int nowPinStatus;
  for(i=0;i<pinNum_;i++){
    nowPinStatus = digitalRead(pinNo_[i]);
    if(pinStatus_[i] != nowPinStatus){
      input = true;
      switch(pinType_[i]){
      case S_PIN_TYPE_STICK:
        // detect change status stick
        switch(pinId_[i]){
        case 2:
          if(nowPinStatus == LOW){
            myJoystickDevice_.setXAxis(-127);
          }else{
            myJoystickDevice_.setXAxis(0);
          }
          break;
        case 3:
          if(nowPinStatus == LOW){
            myJoystickDevice_.setXAxis(127);
          }else{
            myJoystickDevice_.setXAxis(0);
          }
          break;
        case 0:
          if(nowPinStatus == LOW){
            myJoystickDevice_.setYAxis(-127);
          }else{
            myJoystickDevice_.setYAxis(0);
          }
          break;
        case 1:
          if(nowPinStatus == LOW){
            myJoystickDevice_.setYAxis(127);
          }else{
            myJoystickDevice_.setYAxis(0);
          }
          break;
        default:
          break;
        }
        break;
      case S_PIN_TYPE_BUTTON:
        // detect change status button
        if(nowPinStatus == LOW){
          myJoystickDevice_.pressButton(pinId_[i]);
        }else{
          myJoystickDevice_.releaseButton(pinId_[i]);
        }
        break;
      default:
        break;
      }
    }
    pinStatus_[i] = nowPinStatus;
    
  }

  myJoystickDevice_.update();
  
  return( input );  
 }
  
  void flipRapidEnable(){
    int i;
    for(i=0;i<pinNum_;i++){
      switch(pinType_[i]){
      case S_PIN_TYPE_BUTTON:
        // Switch when button is pressed.
        if( digitalRead(pinNo_[i]) == LOW ){
          myJoystickDevice_.flipRapidEnable( pinId_[i] );
        }
        break;
      default:
        break;
      }
    }
  }
};

SixTerminalJoystick myJoystick0(0,2,3,4,5,6,7);
SixTerminalJoystick myJoystick1(1,A0,A1,A2,A3,A4,A5);
int rapidButtonStatus = HIGH;

void setup() {
  pinMode(13, OUTPUT);
  pinMode(12, INPUT_PULLUP);
}

void loop() {
  bool input = false;

  // Check rapid fire control button status.
  int nowRapidButtonStatus = digitalRead(12);
  if( nowRapidButtonStatus != rapidButtonStatus ){
    if( nowRapidButtonStatus == LOW ){
      myJoystick0.flipRapidEnable();
      myJoystick1.flipRapidEnable();
    }
  }
  rapidButtonStatus = nowRapidButtonStatus;

  // Update joystick status.
  input = myJoystick0.update() | myJoystick1.update();

  // Monitoring led control.
  if (input == true)
  {
    digitalWrite(13, 1);
  }
  else
  {
    digitalWrite(13, 0);
  }
}

